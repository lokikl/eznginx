#!/usr/bin/env sh
set -e

echo "Generate bind config from yaml file"
/generate_config.rb

if [[ "$NGINX_UID" != "" ]]; then
  echo "Set uid for nginx to ${NGINX_UID}"
  usermod -u ${NGINX_UID} nginx
fi


# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}
# setup handlers
trap 'kill ${!}; term_handler' SIGTERM

nginx -g "daemon off;" &
pid="$!"

# wait forever
while true; do
  tail -f /dev/null & wait ${!}
done
