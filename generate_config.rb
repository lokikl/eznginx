#!/usr/bin/ruby

require 'yaml'
require 'erb'

if ENV['CONFIG'].to_s == ''
  raise "ENV['CONFIG'] is expected but missing"
end
$config = YAML.load(ENV['CONFIG'])

erb_template = File.read('/nginx.conf.erb')
renderer = ERB.new(erb_template, 0, '-')
nginx_conf = renderer.result
File.write('/etc/nginx/nginx.conf', nginx_conf)
