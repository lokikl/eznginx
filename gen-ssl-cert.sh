#!/usr/bin/env bash

FQDN=$1
if [[ "$FQDN" == "" ]]; then
  echo "USAGE: gen-cert.sh loki.appcara.dev"
  exit 0
fi
openssl req -nodes -newkey rsa:2048 -keyout $FQDN.key -out $FQDN.csr -subj "/C=HK/ST=Hong Kong/L=Hong Kong/O=lokikl/OU=lokikl/CN=$FQDN"
openssl x509 -req -days 365 -in $FQDN.csr -signkey $FQDN.key -out $FQDN.crt
rm $FQDN.csr
